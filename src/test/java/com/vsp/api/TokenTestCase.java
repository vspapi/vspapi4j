package com.vsp.api;

import java.io.FileInputStream;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import junit.framework.TestCase;

public class TokenTestCase extends TestCase {
	
	private Log log;

	private VSContext ctx;
	
	private VSCard card;
	
	protected void setUp() throws Exception {
		 log = LogFactory.getLog(getClass());
		 
		 Properties config = new Properties();
		 config.load(new FileInputStream("./src/main/resources/vscontext.properties"));
		 ctx = new VSContext(config);
		 
		 config.load(new FileInputStream("./src/main/resources/card-amex-sky.properties"));
		 card = new VSCard(config);
	}
	
	public void testTokenLifecycle() throws Exception {
		VSToken token = new VSToken();
		
		log.info("First create a new token...");
		token.create(ctx, card);
		assertNotNull(token.getToken());

		log.info("Now attempt to query the token that was created...");
		token = new VSToken(token.getToken());
		VSCard card2 = new VSCard(card.getExpMonth(), card.getExpYear(), null, null);
		token.query(ctx, card2);
		
		assertEquals(card.getFirstName(), card2.getFirstName());
		assertEquals(card.getLastName(), card2.getLastName());
		
		log.info("Update the expiration info for the card/token");
		card2.setExpMonth("07");
		card2.setExpYear("30");
		token.update(ctx, card2);
		
		log.info("Query the token again and see if the changes got updated.");
		VSCard card3 = new VSCard(card2.getExpMonth(), card2.getExpYear(), null, null);
		token.query(ctx, card3);
		
		assertEquals(card2.getFirstName(), card3.getFirstName());
		assertEquals(card2.getLastName(), card3.getLastName());
		assertEquals(card2.getStreet(), card3.getStreet());
		assertEquals(card2.getZipcode(), card3.getZipcode());
		
		
		
		log.info("Now delete the token and verify that there were no errors.");
		token.delete(ctx);
	}
}
