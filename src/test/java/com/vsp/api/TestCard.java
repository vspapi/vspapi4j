package com.vsp.api;

import java.util.Properties;

public class TestCard {

	public String number;
	public String firstName;
	public String lastName;
	public VSCardType processor;
	public String expYear;
	public String expMonth;
	public String cvv;
	public String street;
	public String zip;

	public TestCard(
			VSCardType processor, 
			String number, 
			String firstName, 
			String lastName, 
			String expMonth,
			String expYear, 
			String cvv, 
			String street, 
			String zip) 
	{
		this.number = number;
		this.firstName = firstName;
		this.lastName = lastName;
		this.processor = processor;
		this.expMonth = expMonth;
		this.expYear = expYear;
		this.cvv = cvv;
	}
	
	public TestCard(Properties p) {
		this.number = p.getProperty("card.number");
		this.firstName = p.getProperty("card.firstName");
		this.lastName = p.getProperty("card.lastName");
		this.processor = VSCardType.valueOf(p.getProperty("card.processor"));
		this.expMonth = p.getProperty("card.expMonth");;
		this.expYear = p.getProperty("card.expYear");;
		this.cvv = p.getProperty("card.cvv");;
	}
}
