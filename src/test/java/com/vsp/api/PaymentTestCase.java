package com.vsp.api;

import java.io.FileInputStream;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.vsp.service.payment.Level3PurchaseInfo;
import com.vsp.service.payment.LineItem;

import junit.framework.TestCase;

public class PaymentTestCase extends TestCase {

	private Log log;

	private VSContext ctx;
	
	private VSCard card;
	
	protected void setUp() throws Exception {
		 log = LogFactory.getLog(getClass());
		 
		 Properties config = new Properties();
		 config.load(new FileInputStream("./src/main/resources/vscontext.properties"));
		 ctx = new VSContext(config);
		 
		 config.load(new FileInputStream("./src/main/resources/card-amex-sky.properties"));
		 card = new VSCard(config);
	}
	
	public void testPaymentLifecycle() throws Exception {
		VSToken token = new VSToken();
		
		log.info("Creating Token");
		token.create(ctx, card);
		
		log.info("Make a payment");
		VSPayment pay = new VSPayment(token);
		pay.setAmount(480.00);
		
		Level3PurchaseInfo l3data = new Level3PurchaseInfo();
		l3data.setAlternateTaxAmount(8.1);
		l3data.setDiscountAmount(12.34);
		l3data.setDutyAmount(1.2);
		l3data.setFreightAmount(32.56);
		l3data.setTaxExempt(true);
		l3data.setOrderDate("20150213");
		
		l3data.addLineItem(new LineItem("123","Flight Training",4.3, "hrs",75));
		l3data.addLineItem(new LineItem("124","Aircraft Rental",2.2, "hrs",480));
		
		
		
		pay.process(ctx, "1234", null, l3data);
	}
	
	public void testPaymentLifecycle2() throws Exception {
		
		log.info("Make a payment");
		VSPayment pay = new VSPayment(card);
		pay.setAmount(480.00);
		
		Level3PurchaseInfo l3data = new Level3PurchaseInfo();
		l3data.setAlternateTaxAmount(8.1);
		l3data.setDiscountAmount(12.34);
		l3data.setDutyAmount(1.2);
		l3data.setFreightAmount(32.56);
		l3data.setTaxExempt(true);
		l3data.setOrderDate("20150213");
		
		l3data.addLineItem(new LineItem("123","Flight Training",4.3, "hrs",75));
		l3data.addLineItem(new LineItem("124","Aircraft Rental",2.2, "hrs",480));
		
		
		
		pay.process(ctx, "1234", null, l3data);
	}

}
