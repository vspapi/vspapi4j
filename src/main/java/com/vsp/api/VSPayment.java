package com.vsp.api;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.vsp.service.payment.IndType;
import com.vsp.service.payment.Level2PurchaseInfo;
import com.vsp.service.payment.Level3PurchaseInfo;
import com.vsp.service.payment.ProcessPaymentReply;
import com.vsp.service.payment.ProcessPaymentRequest;
import com.vsp.service.processrefund.ProcessRefundReply;
import com.vsp.service.processrefund.ProcessRefundRequest;
import com.vsp.service.processvoid.ProcessVoidReply;
import com.vsp.service.processvoid.ProcessVoidRequest;

public class VSPayment {
	
	private String refNumber;
	
	private double amount;
	
	private Date date;
	
	private String receipt;
	
	private VSToken token;
	
	private VSCard card;
	
	public VSPayment(VSToken token) {
		this.token = token;
	}
	
	public VSPayment(VSCard card) {
		this.card = card;
	}
	
	public VSPayment(VSToken token, String refNumber) {
		this.token = token;
		this.refNumber = refNumber;
	}
	
	public String getRefNumber() {
		return refNumber;
	}

	public double getAmount() {
		return amount;
	}

	public Date getDate() {
		return date;
	}

	public String getReceipt() {
		return receipt;
	}

	public VSToken getToken() {
		return token;
	}

	public void setRefNumber(String refNumber) {
		this.refNumber = refNumber;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setReceipt(String receipt) {
		this.receipt = receipt;
	}

	public void setToken(VSToken token) {
		this.token = token;
	}

	/**
	 * This API is used to process a credit card or token transaction on the vSecureProcessing Server.
	 * @param ctx
	 * @throws Exception
	 */
	public void process(VSContext ctx, String invoiceNum, Level2PurchaseInfo l2data, Level3PurchaseInfo l3data) throws Exception {
		ProcessPaymentRequest req = new ProcessPaymentRequest(ctx);
		
		req.setAmount(this.amount);
		if (this.token != null) {
			req.setToken(this.token.getToken());
		} else {
			req.setAccountNumber(card.getNumber());
			req.setExpirationMonth(card.getExpMonth());
			req.setExpirationYear(card.getExpYear());
		}
		req.setApplicationId(ctx.getAppId());
		req.setIndustryType(IndType.ECOM_WITH_SSL, invoiceNum);
		req.setRecurring(false);
		req.setLevel2Data(l2data);
		req.setLevel3Data(l3data);
		
		ProcessPaymentReply reply = req.send(ctx.getGatewayUrl());
		
		System.out.println(req);
		System.out.println(reply);
		
		if (reply.getStatus() != 0) {
			
			if (reply.getAdditionalResponseData() != null)
				throw new Exception(reply.getAdditionalResponseData());
			
			else if (reply.getAuthIdentificationResponse() != null)
				throw new Exception(reply.getAuthIdentificationResponse());
			
			else
				throw new Exception(reply.getResultMessage());
			
		}
		
		this.refNumber = reply.getReferenceNumber();
		this.receipt = reply.getReceipt();
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");	
		this.date = formatter.parse(reply.getTransactionDate());
	}
	
	/**
	 * This API is used to refund a credit card or token transaction on the vSecureProcessing Server.
	 * @param ctx
	 * @throws Exception
	 */
	public void refund(VSContext ctx) throws Exception {
		ProcessRefundRequest req = new ProcessRefundRequest(ctx);
		
		ProcessRefundReply reply = req.send(ctx.getGatewayUrl());
		
		if (reply.getStatus() != 0) {
			throw new Exception(reply.getResultMessage());
		}
	}
	
	/**
	 * This API is used to void a credit card or token transaction on the vSecureProcessing Server.
	 * @param ctx
	 * @throws Exception
	 */
	public void cancel(VSContext ctx) throws Exception {
		ProcessVoidRequest req = new ProcessVoidRequest(ctx);
		
		ProcessVoidReply reply = req.send(ctx.getGatewayUrl());
		
		if (reply.getStatus() != 0) {
			throw new Exception(reply.getResultMessage());
		}
	}
}
