package com.vsp.api;

import java.util.Properties;

public class VSContext {
	private String appId;
	private String userId;
	private String gatewayId;
	private String platform;
	private String gatewayUrl;
	
	public VSContext(Properties props) {
		this.appId = props.getProperty("app.id");
		this.userId = props.getProperty("user.id");
		this.gatewayId = props.getProperty("gateway.id");
		this.gatewayUrl = props.getProperty("gateway.url");
		this.platform = props.getProperty("platform", "Bypass");
	}

	public VSContext(String appId, String userId, String gatewayId, String platform, String gatewayUrl) {
		super();
		this.appId = appId;
		this.userId = userId;
		this.gatewayId = gatewayId;
		this.platform = platform;
		this.gatewayUrl = gatewayUrl;
	}

	public String getAppId() {
		return appId;
	}

	public String getUserId() {
		return userId;
	}

	public String getGatewayId() {
		return gatewayId;
	}

	public String getPlatform() {
		return platform;
	}

	public String getGatewayUrl() {
		return gatewayUrl;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public void setGatewayUrl(String gatewayUrl) {
		this.gatewayUrl = gatewayUrl;
	}
	
}
