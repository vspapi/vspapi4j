package com.vsp.api;

import com.vsp.service.token.CreateTokenReply;
import com.vsp.service.token.CreateTokenRequest;
import com.vsp.service.token.DeleteTokenReply;
import com.vsp.service.token.DeleteTokenRequest;
import com.vsp.service.token.QueryTokenReply;
import com.vsp.service.token.QueryTokenRequest;
import com.vsp.service.token.UpdateTokenRequest;

public class VSToken {
	
	private String token;
	
	private String customId;
	
	public VSToken () {
		super();
	}
	
	public VSToken (String token) {
		this.token = token;
	}
	
	public String getToken() {
		return token;
	}
	
	public String getCustomId() {
		return customId;
	}
	
	public void setCustomId(String customId) {
		this.customId = customId;
	}

	/**
	 * The Create Token message accepts and stores encrypted credit card account information. 
	 * A successful message returns a token that can be used to process future transactions.
	 * 
	 * The token generated is a 16 digit number, uniquely identifying the card information 
	 * within the vSecureProcessing system. The last 4 digits of the token include the last 
	 * 4 digits of the account number.
	 * 
	 * The token is tied to the account that created the token. The token can only be used 
	 * to process a transaction with the account that the token was created for.
	 * 
	 * A token will be store on the vSecureProcessing system for up to 1 year from the last 
	 * time the token was accessed or until the expiration date of the account number is 
	 * reached. 
	 * 
	 * For example, each time a token is accessed (either to process a transaction or with 
	 * an update command), the token expiration date is extended 1 year from that moment.
	 * 
	 * @return VSResponseCode
	 * @throws Exception 
	 */
	public void create(VSContext ctx, VSCard card) throws Exception {
		
		CreateTokenRequest req = new CreateTokenRequest(ctx);
		
		req.setApplicationId(ctx.getAppId());
		req.setAccountNumber(card.getNumber());
		req.setAvsStreet(card.getStreet());
		req.setAvsZip(card.getZipcode());
		req.setCardHolderFirstName(card.getFirstName());
		req.setCardHolderLastName(card.getLastName());
		req.setExpirationMonth(card.getExpMonth());
		req.setExpirationYear(card.getExpYear());
		
		CreateTokenReply reply = req.send(ctx.getGatewayUrl());
		
		if (reply.getStatus() != 0) {
			throw new Exception(reply.getResultMessage());
		}
		
		this.token = reply.getToken();
	}
	
	/**
	 * The Delete Token message processes and removes the given token from the Token 
	 * Database. 
	 * @return VSResponseCode
	 */
	public void delete(VSContext ctx) throws Exception {
		DeleteTokenRequest req = new DeleteTokenRequest(ctx);
		
		req.setToken(getToken());
		
		DeleteTokenReply reply = req.send(ctx.getGatewayUrl());
		
		if (reply.getStatus() != 0) {
			throw new Exception(reply.getResultMessage());
		}
	}
	
	/**
	 * The Query Token message retrieves a list of token data that matches the request 
	 * parameters.  
	 * @return
	 */
	public void query(VSContext ctx, VSCard card) throws Exception {
		QueryTokenRequest req = new QueryTokenRequest(ctx);
		
		req.setCustomId(this.getCustomId());
		req.setCardType(card.getType());
		req.setExpirationMonth(card.getExpMonth());
		req.setExpirationYear(card.getExpYear());
		req.setFirstName(card.getFirstName());
		req.setLastName(card.getLastName());
		
		QueryTokenReply reply = req.send(ctx.getGatewayUrl());
		
		card.setType(reply.getCardType());
		card.setExpMonth(reply.getExpirationMonth());
		card.setExpYear(reply.getExpirationYear());
		card.setFirstName(reply.getFirstName());
		card.setLastName(reply.getLastName());
		this.customId = reply.getCustomId();
		
		this.token = reply.getToken();
		if (reply.getStatus() != 0) {
			throw new Exception(reply.getResultMessage());
		}
		
	}
	
	/**
	 * The Update Expiration Request message updates the card data expiration data 
	 * associated with the given token, based on the parameters supplied.
	 * @return
	 */
	public void update(VSContext ctx, VSCard card) throws Exception {
		UpdateTokenRequest req = new UpdateTokenRequest(ctx);
		
		req.setApplicationId(ctx.getAppId());
		req.setAvsStreet(card.getStreet());
		req.setAvsZip(card.getZipcode());
		req.setExpirationMonth(card.getExpMonth());
		req.setExpirationYear(card.getExpYear());
		req.setToken(getToken());
		
		CreateTokenReply reply = req.send(ctx.getGatewayUrl());
		
		if (reply.getStatus() != 0) {
			throw new Exception(reply.getResultMessage());
		}
		
		this.token = reply.getToken();
	}
	
	/**
	 * Returns the VSToken object in its String representation.
	 */
	public String toString() {
		return token;
	}
}
