package com.vsp.api;

import java.util.Properties;

public class VSCard {

	private VSCardType type;
	
	private String number;

	private String expMonth;

	private String expYear;

	private String firstName;

	private String lastName;

	private String street;

	private String zipcode;
	
	private String cvv;
	
	public VSCard(String expMonth, String expYear, String firstName, String lastName) {
		this.expMonth = expMonth;
		this.expYear = expYear;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public VSCard(Properties p) {
		this.number = p.getProperty("card.number");
		this.firstName = p.getProperty("card.firstName");
		this.lastName = p.getProperty("card.lastName");
		this.type = VSCardType.valueOf(p.getProperty("card.processor"));
		this.expMonth = p.getProperty("card.expMonth");;
		this.expYear = p.getProperty("card.expYear");;
		this.cvv = p.getProperty("card.cvv");;
	}

	public VSCard(VSCardType type, String number, String expMonth, String expYear, String firstName, String lastName,
			String street, String zipcode, String cvv) {
		super();
		this.type = type;
		this.number = number;
		this.expMonth = expMonth;
		this.expYear = expYear;
		this.firstName = firstName;
		this.lastName = lastName;
		this.street = street;
		this.zipcode = zipcode;
		this.cvv = cvv;
	}

	public VSCardType getType() {
		return type;
	}

	public String getNumber() {
		return number;
	}

	public String getExpMonth() {
		return expMonth;
	}

	public String getExpYear() {
		return expYear;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getStreet() {
		return street;
	}

	public String getZipcode() {
		return zipcode;
	}

	public String getCvv() {
		return cvv;
	}

	public void setType(String type) {
		if(type != null) {
			this.type = VSCardType.valueOf(type);
		}
		
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public void setExpMonth(String expMonth) {
		this.expMonth = expMonth;
	}

	public void setExpYear(String expYear) {
		this.expYear = expYear;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	
	
	
}
