package com.vsp.api;

public enum VSCardType {
	AMEX ("Amex"),
	VISA ("Visa"),
	DSCV ("DSCV"),
	MC ("MC");
	
	private String value;
	
	private VSCardType(String value) {
		this.value = value;
	}
	
	public String toString() {
		return this.value;
	}
}
