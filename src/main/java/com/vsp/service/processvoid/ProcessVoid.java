package com.vsp.service.processvoid;

import javax.xml.bind.annotation.XmlElement;

class ProcessVoid {
	
	@XmlElement(name="ReferenceNumber")
	String referenceNumber;
	
	@XmlElement(name="ApplicationId")
	String applicationId;
	
}
