package com.vsp.service.processvoid;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.vsp.api.VSContext;
import com.vsp.service.VSRequest;

@XmlRootElement(name="Request")
public class ProcessVoidRequest extends VSRequest {
	
	@XmlElement(name="ProcessVoid")
	ProcessVoid processVoid;

	public ProcessVoidRequest() {
		super();
		this.processVoid = new ProcessVoid();
	}

	public ProcessVoidRequest(VSContext ctx) {
		super(ctx);
		this.processVoid = new ProcessVoid();
	}

	@XmlTransient
	@Override
	public String getPath() {
		return "/vsg2/processvoid";
	}
	
	public void validate() {
		//!! Needs to be implemented
	}
	
	@XmlTransient
	public String getReferenceNumber() {
		return processVoid.referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.processVoid.referenceNumber = referenceNumber;
	}

	@XmlTransient
	public String getApplicationId() {
		return processVoid.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.processVoid.applicationId = applicationId;
	}
	
	public ProcessVoidReply send(String url) throws Exception {
		
		JAXBContext jaxbContext = JAXBContext.newInstance(ProcessVoidReply.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		
		return (ProcessVoidReply) jaxbUnmarshaller.unmarshal(new StringReader(doPost(url)));
	}

}
