package com.vsp.service;

public class InvalidSizeException extends RuntimeException {

	private static final long serialVersionUID = 6592443890036476284L;

	public InvalidSizeException(String fieldname, String fieldvalue, int maxSize) {
		super(String.format("Field [%s=%s] is too large. Max size is %d ", fieldname, fieldvalue, maxSize));
	}
	
	public InvalidSizeException(String fieldvalue, int maxSize) {
		super(String.format("Field value [%s] is too large. Max size is %d ", fieldvalue, maxSize));
	}

}
