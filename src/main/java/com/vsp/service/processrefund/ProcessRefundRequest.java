package com.vsp.service.processrefund;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.vsp.api.VSContext;
import com.vsp.service.VSRequest;

@XmlRootElement(name="Request")
public class ProcessRefundRequest extends VSRequest {

	@XmlElement(name="ProcessRefund")
	ProcessRefund processRefund;
	
	public ProcessRefundRequest() {
		super();
		this.processRefund = new ProcessRefund();
	}

	public ProcessRefundRequest(VSContext ctx) {
		super(ctx);
		this.processRefund = new ProcessRefund();
	}


	@XmlTransient
	@Override
	public String getPath() {
		return "/vsg2/processrefund";
	}
	
	public void validate() {
		//!! Needs to be implemented
	}
	
	@XmlTransient
	public String getAmount() {
		return this.processRefund.amount;
	}

	public void setAmount(String amount) {
		this.processRefund.amount = amount;
	}

	@XmlTransient
	public String getAccountNumber() {
		return processRefund.accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.processRefund.accountNumber = accountNumber;
	}

	@XmlTransient
	public String getExpirationMonth() {
		return processRefund.expirationMonth;
	}

	public void setExpirationMonth(String expirationMonth) {
		this.processRefund.expirationMonth = expirationMonth;
	}

	@XmlTransient
	public String getExpirationYear() {
		return processRefund.expirationYear;
	}

	public void setExpirationYear(String expirationYear) {
		this.processRefund.expirationYear = expirationYear;
	}

	@XmlTransient
	public String getApplicationId() {
		return processRefund.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.processRefund.applicationId = applicationId;
	}

	@XmlTransient
	public String getToken() {
		return processRefund.token;
	}

	public void setToken(String token) {
		this.processRefund.token = token;
	}
	
	public ProcessRefundReply send(String url) throws Exception {
		JAXBContext jaxbContext = JAXBContext.newInstance(ProcessRefundReply.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		
		String resp = doPost(url);
		
		return (ProcessRefundReply) jaxbUnmarshaller.unmarshal(new StringReader(resp));
	}

}
