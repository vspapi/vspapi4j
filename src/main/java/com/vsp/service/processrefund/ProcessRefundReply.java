package com.vsp.service.processrefund;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.vsp.service.VSReply;

@XmlRootElement(name="Reply")
public class ProcessRefundReply extends VSReply {

	/**
	 * Identifies the response identification assigned by the authorizing institution. Present only for approvals.
	 */
	@XmlElement(name="AuthIdentificationResponse")
	String authIdentResponse;
	
	/**
	 * Unique value identifying the transaction. Used to identify the transaction for void/reversals
	 */
	@XmlElement(name="ReferenceNumber")
	String referenceNumber;
	
	/**
	 * Identifies the transaction's total amount in US dollars. Format assumes 2 decimal points.
	 */
	@XmlElement(name="TransactionAmount")
	String transactionAmount;
	
	/**
	 * Identifies the transaction transmission date and time. Format: MMDDhhmmss
	 */
	@XmlElement(name="TransactionDate")
	String transactionDate;
	
	/**
	 * Receipt of transaction. Formatted to fit a 30 column receipt printer.
	 */
	@XmlElement(name="Receipt")
	String receipt;

}
