package com.vsp.service.processrefund;

import javax.xml.bind.annotation.XmlElement;

class ProcessRefund {

	@XmlElement(name="Amount")
	String amount;
	
	@XmlElement(name="AccountNumber")
	String accountNumber;
	
	@XmlElement(name="ExpirationMonth")
	String expirationMonth;
	
	@XmlElement(name="ExpirationYear")
	String expirationYear;
	
	@XmlElement(name="ApplicationId")
	String applicationId;
	
	@XmlElement(name="Token")
	String token;

}
