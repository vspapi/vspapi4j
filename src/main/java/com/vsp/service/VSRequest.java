package com.vsp.service;

import java.io.StringWriter;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlElement;

import com.vsp.api.VSContext;

public abstract class VSRequest {
	
	@XmlElement(name="MerchantData")
	private MerchantData merchantData;
	
	public VSRequest() {
		super();
	}
	
	public VSRequest(VSContext ctx) {
		this.merchantData = new MerchantData(ctx);
	}

	public abstract String getPath();
	
	public abstract void validate();

	public String toString() {
		try {
			StringWriter writer = new StringWriter();
			toString(writer);
			return writer.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}

	public void toString(Writer writer) throws Exception {
		JAXBContext jaxbContext = JAXBContext.newInstance(getClass());

		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		jaxbMarshaller.marshal(this, writer);
	}

	public String doPost(String url) throws Exception {
		
		validate();
		
		MultipartUtility multipart = new MultipartUtility(url + getPath(), "UTF-8");

		multipart.addHeaderField("User-Agent", "SierraSkyport");

		multipart.addFormField("param", toString());

		List<String> response = multipart.finish();

		System.out.println("SERVER REPLIED:");
		StringBuffer buffer = new StringBuffer();
		for (String line : response) {
			buffer.append(line);
		}
		return buffer.toString();
	}
	
	public static String chop(String s, int maxSize) {
		return s.substring(0, Math.min(s.length(), maxSize));
	}
	
	public static String check(String s, int maxSize) {
		if (s.length()<=maxSize)
			return s;
		throw new InvalidSizeException(s, maxSize);
	}
	
	public static String check(String fieldName, String fieldValue, int maxSize) {
		if (fieldValue == null) 
			throw new RuntimeException(fieldName + " is null");
			
		if (fieldValue.length()>maxSize)
			throw new InvalidSizeException(fieldName, fieldValue, maxSize);
		
		return fieldValue;
		
	}
	
	public static String formatDecimal(double num, int decimals) {
		DecimalFormat format = new DecimalFormat("#");
		format.setMinimumFractionDigits(decimals);
		return format.format(num).replace(".", "");
	}
}
