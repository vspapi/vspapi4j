package com.vsp.service;

import java.io.StringWriter;
import java.io.Writer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlElement;


public class VSReply {
	
	@XmlElement(name="Status")
	int status;
	
	@XmlElement(name="ResultMessage")
	String resultMessage;
	
	
	/**
	 * ????
	 * @return
	 */
	public String getResultMessage() {
		return resultMessage;
	}
	
	/**
	 * Identifies the status of the transaction. Values: 0 - Successful; 1 - Unsuccessful
	 * @return
	 */
	public int getStatus() {
		return status;
	}
	
	public String toString() {
		try {
			StringWriter writer = new StringWriter();
			toString(writer);
			return writer.toString();
		} catch (Exception e) {
			return e.getMessage();
		}
	} 
	
	public void toString(Writer writer) throws Exception {
		JAXBContext jaxbContext = JAXBContext.newInstance(getClass());
		
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		
		jaxbMarshaller.marshal(this, writer);
	}

}
