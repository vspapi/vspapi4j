package com.vsp.service;

import javax.xml.bind.annotation.XmlElement;

import com.vsp.api.VSContext;


public class MerchantData {
	@XmlElement(name="UserId")
	String userId;
	
	@XmlElement(name="GID")
	String gatewayId;
	
	@XmlElement(name="Platform")
	String platform;
	
	@XmlElement(name="Tid")
	String terminalId;
	
	public MerchantData() {
		this.platform = "Buypass";
		this.terminalId = "01";
	}
	
	public MerchantData(VSContext ctx) {
		this.userId = ctx.getUserId();
		this.gatewayId = ctx.getGatewayId();
		this.platform = ctx.getPlatform();
		this.terminalId = "01";
	}

	public String getUserId() {
		return userId;
	}

	public String getGatewayId() {
		return gatewayId;
	}

	public String getPlatform() {
		return platform;
	}

	public String getTerminalId() {
		return terminalId;
	}
	
	
}
