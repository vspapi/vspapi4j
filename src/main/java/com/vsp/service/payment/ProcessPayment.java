package com.vsp.service.payment;

import javax.xml.bind.annotation.XmlElement;

class ProcessPayment {
	
	@XmlElement(name="AccountNumber")
	String accountNumber;
	
	@XmlElement(name="Amount")
	String amount;
	
	@XmlElement(name="ApplicationId")
	String applicationId;
	
	@XmlElement(name="AvsStreet")
	String avsStreet;
	
	@XmlElement(name="AvsZip")
	String avsZip;
	
	@XmlElement(name="CardHolderFirstName")
	String firstName;
	
	@XmlElement(name="CardHolderLastName")
	String lastName;
	
	@XmlElement(name="Cf1")
	String customField1;
	
	@XmlElement(name="Cf2")
	String customField2;
	
	@XmlElement(name="Cf3")
	String customField3;
	
	@XmlElement(name="Cvv")
	String cvv;
	
	@XmlElement(name="ExpirationMonth")
	String expirationMonth;
	
	@XmlElement(name="ExpirationYear")
	String expirationYear;
	
	@XmlElement(name="IndustryType")
	IndustryType industryType;
	
	@XmlElement(name="Token")
	String token;
	
	@XmlElement(name="TypeOfSale")
	String typeOfSale;

	@XmlElement(name="Recurring")
	int recurring;
	
	
	
}
