package com.vsp.service.payment;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.vsp.service.VSReply;

@XmlRootElement(name="Reply")
public class ProcessPaymentReply extends VSReply {
	
	@XmlElement(name="AuthIdentificationResponse")
	String authIdentificationResponse;
	
	@XmlElement(name="ReferenceNumber")
	String referenceNumber;
	
	@XmlElement(name="TransactionNumber")
	String transactionNumber;
	
	@XmlElement(name="TransactionAmount")
	String transactionAmount;
	
	@XmlElement(name="TransactionDate")
	String transactionDate;
	
	@XmlElement(name="AvsResponse")
	String avsResponse;
	
	@XmlElement(name="CvvResponse")
	String cvvResponse;
	
	@XmlElement(name="Receipt")
	String receipt;
	
	@XmlElement(name="AdditionalResponseData")
	String additionalResponseData;

	public ProcessPaymentReply() {
		super();
	}

	public String getAuthIdentificationResponse() {
		return authIdentificationResponse;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public String getTransactionNumber() {
		return transactionNumber;
	}

	public String getTransactionAmount() {
		return transactionAmount;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public String getAvsResponse() {
		return avsResponse;
	}

	public String getCvvResponse() {
		return cvvResponse;
	}

	public String getReceipt() {
		return receipt;
	}

	public String getAdditionalResponseData() {
		return additionalResponseData;
	}
	
	

}
