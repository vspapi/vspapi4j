package com.vsp.service.payment;

public enum IndType {
	
	// Mail Order / Telephone Order (MOTO)
	MOTO ("moto"),
	// eCommerce secured using Verified by Visa or 3DES
	ECOM_Verified_3DES ("ecom_1"),
	// eCommerce secured with attempted 3DES
	ECOM_Attempted_3DES ("ecom_2"),
	// eCommerce secured using SSL
	ECOM_WITH_SSL ("ecom_3"),
	// eCommerce non-secured
	ECOM_NON_SECURED ("ecom_4");
	
	private String value;
	
	private IndType(String value) {
		this.value = value;
	}
	
	public String toString() {
		return value;
	}
}
