package com.vsp.service.payment;

import javax.xml.bind.annotation.XmlElement;

import com.vsp.api.VSCardType;
import com.vsp.service.VSRequest;

public class Level2PurchaseInfo {
	
	@XmlElement(name="Level2CardType")
	String processor;
	
	@XmlElement(name="PurchaseCode")
	String purchaseCode;
	
	@XmlElement(name="ShipToCountryCode")
	String shipToCountryCode;
	
	@XmlElement(name="ShipToPostalCode")
	String shipToPostalCode;
	
	@XmlElement(name="ShipFromPostalCode")
	String shipFromPostalCode;
	
	@XmlElement(name="SalesTax")
	String salesTax;
	
	@XmlElement(name="ProductDescription1")
	String productDescription1;
	
	@XmlElement(name="ProductDescription2")
	String productDescription2;
	
	@XmlElement(name="ProductDescription3")
	String productDescription3;
	
	@XmlElement(name="ProductDescription4")
	String productDescription4;
	
	Level2PurchaseInfo() {
		super();
	}

	public void setProcessor(VSCardType processor) {
		this.processor = processor.toString();
	}

	/**
	 * Purchase order code. For AMEX, only the first 10 characters 
	 * are sent to American Express.
	 * @param purchaseCode
	 */
	public void setPurchaseCode(String purchaseCode) {
		this.purchaseCode = VSRequest.chop(purchaseCode, 16);
	}

	/**
	 * Destination country code. See appendix TBD for code listing.
	 * @param shipToCountryCode
	 */
	public void setShipToCountryCode(String shipToCountryCode) {
		this.shipToCountryCode = VSRequest.check("shipToCountryCode", shipToCountryCode, 3);
	}

	/**
	 * Destination postal code. For AMEX, only the first 6 characters 
	 * are sent to American Express
	 * @param shipToPostalCode
	 */
	public void setShipToPostalCode(String shipToPostalCode) {
		this.shipToPostalCode = VSRequest.chop(shipToPostalCode, 10);
	}

	/**
	 * Source postal code.
	 * @param shipFromPostalCode
	 */
	public void setShipFromPostalCode(String shipFromPostalCode) {
		this.shipFromPostalCode = VSRequest.check("shipFromPostalCode", shipFromPostalCode, 10);
	}

	/**
	 * !! Format: $$$$$cc
	 * @param salesTax
	 */
	public void setSalesTax(String salesTax) {
		this.salesTax = salesTax;
	}

	/**
	 * First product description
	 * @param productDescription1
	 */
	public void setProductDescription1(String productDescription1) {
		this.productDescription1 = VSRequest.chop(productDescription1, 40);
	}
	/**
	 * Second product description
	 * @param productDescription1
	 */
	public void setProductDescription2(String productDescription2) {
		this.productDescription2 = VSRequest.chop(productDescription2, 40);
	}
	/**
	 * Third product description
	 * @param productDescription1
	 */
	public void setProductDescription3(String productDescription3) {
		this.productDescription3 = VSRequest.chop(productDescription3, 40);
	}
	/**
	 * Fourth product description
	 * @param productDescription1
	 */
	public void setProductDescription4(String productDescription4) {
		this.productDescription4 = VSRequest.chop(productDescription4, 40);
	}
	
	
	
}
