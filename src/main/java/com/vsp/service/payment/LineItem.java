package com.vsp.service.payment;

import javax.xml.bind.annotation.XmlElement;

import com.vsp.service.VSRequest;

public class LineItem {
	@XmlElement(name="ItemSequenceNumber")
	String sequenceNumber;
	
	@XmlElement(name="ItemCode")
	String code;
	
	@XmlElement(name="ItemDescription")
	String description;
	
	@XmlElement(name="ItemQuantity")
	String quantity;
	
	@XmlElement(name="ItemUnitOfMeasure")
	String unitOfMeasure;
	
	@XmlElement(name="ItemUnitCost")
	String unitCost;
	
	@XmlElement(name="ItemAmount")
	String amount;
	
	@XmlElement(name="ItemDiscountAmount")
	String discountAmount;
	
	@XmlElement(name="ItemTaxAmount")
	String taxAmount;
	
	@XmlElement(name="ItemTaxRate")
	String taxRate;
	
	public LineItem(String code, String desc, double qty, String uom, double unitCost) {
		setCode(code);
		setQuantity(qty);
		setUnitOfMeasure(uom);
		setUnitCost(unitCost);
		setAmount(unitCost * qty);
	}
	
	public LineItem(String code, String desc, double qty, String uom, double unitCost, double taxRate) {
		this(code, desc, qty, uom, unitCost);
		setTaxRate(taxRate);
		setTaxAmount((unitCost * qty)*(taxRate/100));
	}

	public String getSequenceNumber() {
		return sequenceNumber;
	}

	void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = String.format("%03d", sequenceNumber);;
	}

	public void setCode(String code) {
		this.code = VSRequest.check("code", code, 12);
	}

	public void setDescription(String description) {
		this.description = VSRequest.chop(description, 40);
	}

	public void setQuantity(double quantity) {
		this.quantity = VSRequest.check("quantity", VSRequest.formatDecimal(quantity, 4), 12);
	}

	public void setUnitOfMeasure(String unitOfMeasure) {
		this.unitOfMeasure = VSRequest.chop(unitOfMeasure, 40);
	}

	public void setUnitCost(double unitCost) {
		this.unitCost = VSRequest.check("unitCost", VSRequest.formatDecimal(unitCost, 4), 12);
	}

	public void setAmount(double amount) {
		this.amount = VSRequest.check("amount", VSRequest.formatDecimal(amount, 2), 12);
	}

	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = VSRequest.check("discountAmount", VSRequest.formatDecimal(discountAmount, 2), 12);
	}

	public void setTaxAmount(double taxAmount) {
		this.taxAmount = VSRequest.check("taxAmount", VSRequest.formatDecimal(taxAmount, 2), 12);
	}

	public void setTaxRate(double taxRate) {
		this.taxRate = VSRequest.check("taxRate", VSRequest.formatDecimal(taxRate, 3), 12);
	}
	
	
}
