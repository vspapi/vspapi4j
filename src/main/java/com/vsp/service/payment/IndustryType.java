package com.vsp.service.payment;

import javax.xml.bind.annotation.XmlElement;

public class IndustryType {
	
	@XmlElement(name="IndType")
	String type;
	
	@XmlElement(name="IndInvoice")
	String invoice;
	
	public IndustryType() {
		type = IndType.ECOM_NON_SECURED.toString();
		invoice = "Unassigned";
	}
	
	public IndustryType(IndType type, String invoice) {
		this.type = type.toString();
		this.invoice = invoice;
	}
}
