package com.vsp.service.payment;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;

import com.vsp.service.VSRequest;

public class Level3PurchaseInfo {
	
	
	String purchaseOrderNumber;
	
	
	String orderDate;
	
	@XmlElement(name="DutyAmount")
	String dutyAmount;
	
	@XmlElement(name="AlternateTaxAmount")
	String alternateTaxAmount;
	
	@XmlElement(name="DiscountAmount")
	String discountAmount;
	
	@XmlElement(name="FreightAmount")
	String freightAmount;
	
	@XmlElement(name="TaxExemptFlag")
	boolean taxExemptFlag;
	
	public Level3PurchaseInfo() {
		super();
	}

	@XmlElementWrapper(name="PurchaseItems")
	@XmlElements(@XmlElement(name="LineItem", type=LineItem.class))
	List<LineItem> purchaseItems = new ArrayList<LineItem>();
	
	public void addLineItem(LineItem item) {
		item.setSequenceNumber(purchaseItems.size()+1);
		purchaseItems.add(item);
	}
	
	@XmlElement(name="LineItemCount")
	public int getLineItemCount() {
		return purchaseItems.size();
	}
	
	@XmlElement(name="PurchaseOrderNumber")
	public String getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}

	@XmlElement(name="OrderDate")
	public String getOrderDate() {
		return orderDate;
	}

	public String getDutyAmount() {
		return dutyAmount;
	}

	public String getAlternateTaxAmount() {
		return alternateTaxAmount;
	}

	public String getDiscountAmount() {
		return discountAmount;
	}

	public String getFreightAmount() {
		return freightAmount;
	}

	public boolean isTaxExempt() {
		return taxExemptFlag;
	}

	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public void setDutyAmount(double amount) {
		this.dutyAmount = VSRequest.formatDecimal(amount, 2);
	}

	public void setAlternateTaxAmount(double amount) {
		this.alternateTaxAmount = VSRequest.formatDecimal(amount, 2);
	}

	public void setDiscountAmount(double amount) {
		this.discountAmount = VSRequest.formatDecimal(amount, 2);;
	}

	public void setFreightAmount(double amount) {
		this.freightAmount = VSRequest.formatDecimal(amount, 2);;
	}

	public void setTaxExempt(boolean taxExemptFlag) {
		this.taxExemptFlag = taxExemptFlag;
	}	
	
}
