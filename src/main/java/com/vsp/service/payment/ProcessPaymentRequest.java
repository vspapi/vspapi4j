package com.vsp.service.payment;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.vsp.api.VSContext;
import com.vsp.service.VSRequest;

@XmlRootElement(name="Request")
public class ProcessPaymentRequest extends VSRequest {
	
	@XmlElement(name="ProcessPayment")
	ProcessPayment processPayment;
	
	@XmlElement(name="Level2PurchaseInfo")
	Level2PurchaseInfo level2Data;
	
	@XmlElement(name="Level3PurchaseInfo")
	Level3PurchaseInfo level3Data;

	public ProcessPaymentRequest() {
		super();
		processPayment = new ProcessPayment();
	}

	public ProcessPaymentRequest(VSContext ctx) {
		super(ctx);
		processPayment = new ProcessPayment();
	}


	@XmlTransient
	@Override
	public String getPath() {
		return "/vsg2/processpayment";
	}

	public void validate() {
		//!! Needs to be implemented
	}
	
	public void setLevel2Data(Level2PurchaseInfo level2Data) {
		this.level2Data = level2Data;
	}
	
	public void setLevel3Data(Level3PurchaseInfo level3Data) {
		this.level3Data = level3Data;
	}
	
	/**
	 * Credit card number. Only numeric characters are allowed; spaces 
	 * and hyphens are not allowed; for example; 41111111111111111 is 
	 * allowed, if 4111-1111-1111-1111 is used, the dashes will be automatically
	 * removed from the string.
	 * @param accountNumber
	 */
	public void setAccountNumber(String accountNumber) {
		this.processPayment.accountNumber = accountNumber.replace("-", "");
	}
	
	/**
	 * Full amount of transaction including cents. Only numeric characters 
	 * and a decimal point are allowed; for example, 1000.00
	 * @param amount
	 */
	public void setAmount(double amount) {
		this.processPayment.amount = formatDecimal(amount, 2);
	}

	/**
	 * Application identifier for the application used in sending/receiving 
	 * transaction request. The value of this field is assigned/authorized 
	 * by the gateway and must be used in all transactions used by the 
	 * certified application.
	 * @param applicationId
	 */
	public void setApplicationId(String applicationId) {
		this.processPayment.applicationId = check("applicationId", applicationId, 20);
	}

	/**
	 * Optional. Billing street address used for AVS.
	 * @param avsStreet
	 */
	public void setAvsStreet(String avsStreet) {
		this.processPayment.avsStreet = chop(avsStreet, 0);;
	}

	/**
	 * Optional. Billing ZIP or postal code used for AVS.
	 * @param avsZip
	 */
	public void setAvsZip(String avsZip) {
		this.processPayment.avsZip = check("avsZip", avsZip, 10);
	}

	/**
	 * Optional. Card holder first name.
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.processPayment.firstName = chop(firstName, 40);;
	}

	/**
	 * Optional. Card holder last name.
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.processPayment.lastName = chop(lastName, 40);;
	}

	/**
	 * Optional. Custom field used to record transaction details.
	 * @param customField1
	 */
	public void setCustomField1(String customField1) {
		this.processPayment.customField1 = chop(customField1, 50);
	}

	/**
	 * Optional. Custom field used to record transaction details.
	 * @param customField1
	 */
	public void setCustomField2(String customField2) {
		this.processPayment.customField2 = chop(customField2, 50);
	}

	/**
	 * Optional. Custom field used to record transaction details.
	 * @param customField1
	 */
	public void setCustomField3(String customField3) {
		this.processPayment.customField3 = chop(customField3, 50);
	}

	/**
	 * Card validation value. Retention of this value is <b>strictly prohibited</b>.
	 * @param expirationMonth
	 */
	public void setCVV(String cvv) {
		this.processPayment.cvv = check("cvv", cvv, 4);;
	}
	
	/**
	 * Credit card expiration month in MM format. Exactly two characters required; for example, 02
	 * @param expirationMonth
	 */
	public void setExpirationMonth(String expirationMonth) {
		this.processPayment.expirationMonth = chop(expirationMonth, 2);
	}
	
	/**
	 * Credit card expiration year in YY format. Exactly two characters required; for example, 14.
	 * @param expirationYear
	 */
	public void setExpirationYear(String expirationYear) {
		this.processPayment.expirationYear = chop(expirationYear, 2);
	}

	/**
	 * Optional. Required for eCommerce and MOTO transaction.
	 * @param type
	 * @param invoiceNum
	 */
	public void setIndustryType(IndType type, String invoiceNum) {
		this.processPayment.industryType = new IndustryType(type, invoiceNum);
	}

	/**
	 * Token identifying the card number to process
	 * @param token
	 */
	public void setToken(String token) {
		this.processPayment.token = check("token", token, 16);
	}

	/**
	 * Optional. String identifying type of transaction being processed. 
	 * This field is optional and used for transaction reporting
	 * @param typeOfSale
	 */
	public void setTypeOfSale(String typeOfSale) {
		this.processPayment.typeOfSale = chop(typeOfSale, 20);
	}
	
	/**
	 * Recurring payment indicator. true = ON; false = OFF; Buypass currently 
	 * only supports the recurring payment indicator for Visa and MasterCard transactions.
	 * @param isRecurring
	 */
	public void setRecurring(boolean isRecurring) {
		this.processPayment.recurring = (isRecurring)? 1 : 0;
	}
	
	public ProcessPaymentReply send(String url) throws Exception {
		JAXBContext jaxbContext = JAXBContext.newInstance(ProcessPaymentReply.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		
		String resp = doPost(url);
		
		return (ProcessPaymentReply) jaxbUnmarshaller.unmarshal(new StringReader(resp));
	}

}
