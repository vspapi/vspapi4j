package com.vsp.service;

import com.vsp.api.VSCardType;

public enum CVVResponseCode {

	AMEX_CID_MATCH (VSCardType.AMEX, "Y"),
	AMEX_CID_NON_MATCH (VSCardType.AMEX, "N"),
	AMEX_CID_NOT_CHECKED (VSCardType.AMEX, "U"),
	AMEX_NOT_AVAILABLE (VSCardType.AMEX, "X"),
	
	DSCV_CID_MATCH (VSCardType.DSCV, "M"),
	DSCV_CID_NON_MATCH (VSCardType.DSCV, "N"),
	DSCV_NOT_PROCESSED (VSCardType.DSCV, "P"),
	DSCV_CID_NOT_PRESENT (VSCardType.DSCV, "S"),
	DSCV_ISSUER_NOT_CERTIFIED (VSCardType.DSCV, "U"),
	DSCV_NOT_AVAILABLE (VSCardType.DSCV, "X"),
	
	MC_CVC2_MATCH (VSCardType.MC, "M"),
	MC_CVC2_INVALID (VSCardType.MC, "N"),
	MC_CVC2_NOT_PROCESSED (VSCardType.MC, "P"),
	MC_ISSUER_UNREGISTERED (VSCardType.MC, "U"),
	MC_NOT_AVAILABLE (VSCardType.MC, "X"),
	
	VISA_CVV2_MATCH (VSCardType.VISA, "M"),
	VISA_CVV2_NO_MATCH (VSCardType.VISA, "N"),
	VISA_NOT_PROCESSED (VSCardType.VISA, "P"),
	VISA_CVV2_NOT_PRESENT (VSCardType.VISA, "S"),
	VISA_ISSUER_NOT_CERTIFIED (VSCardType.VISA, "U"),
	VISA_NOT_AVAILABLE (VSCardType.VISA, "X");
	
	
	VSCardType processor;
	String value;
	
	private CVVResponseCode (VSCardType processor, String value) {
		this.processor = processor;
		this.value = value;
	}
	
	public String toString() {
		return this.value;
	}
}
