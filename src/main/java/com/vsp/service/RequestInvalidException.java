package com.vsp.service;

public class RequestInvalidException extends RuntimeException {

	private static final long serialVersionUID = -6822873906263251985L;

	public RequestInvalidException(String message, Throwable cause) {
		super(message, cause);
	}

	public RequestInvalidException(String message) {
		super(message);
	}
	
	

}
