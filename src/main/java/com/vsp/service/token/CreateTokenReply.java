package com.vsp.service.token;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.vsp.service.VSReply;

@XmlRootElement(name="Reply")
public class CreateTokenReply extends VSReply{
	
	
	@XmlElement(name="Token")
	String token;
	
	@XmlElement(name="TokenId")
	String tokenId;
	
	
	@XmlElement(name="ResponseCode")
	String respCode;


	/**
	 * The token that was created in the system.
	 * @return
	 */
	public String getToken() {
		return token;
	}
	
	/**
	 * Identifies the disposition of a message.
	 */
	public String getResponseCode() {
		return respCode;
	}

	/**
	 * ????
	 * @return
	 */
	public String getTokenId() {
		return tokenId;
	}
	

	

	
}
