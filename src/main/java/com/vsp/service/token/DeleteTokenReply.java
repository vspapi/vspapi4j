package com.vsp.service.token;

import javax.xml.bind.annotation.XmlRootElement;

import com.vsp.service.VSReply;

@XmlRootElement(name="Reply")
public class DeleteTokenReply extends VSReply {

}
