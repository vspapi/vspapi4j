package com.vsp.service.token;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.vsp.api.VSContext;
import com.vsp.service.VSRequest;

@XmlRootElement(name="Request")
public class DeleteTokenRequest extends VSRequest {
	
	@XmlElement(name="DeleteToken")
	DeleteToken deleteToken;

	public DeleteTokenRequest() {
		super();
	}
	
	public DeleteTokenRequest(VSContext ctx) {
		super(ctx);
	}
	
	@XmlTransient
	@Override
	public String getPath() {
		return "/vsg2/deletetoken";
	}
	
	public void validate() {
		//!! Needs to be implemented
	}
	
	@XmlTransient
	public String getToken() {
		return (deleteToken == null)? null : deleteToken.token;
	}
	
	public void setToken(String token) {
		deleteToken = new DeleteToken(token);
	}

	public DeleteTokenReply send(String url) throws Exception {
		
		JAXBContext jaxbContext = JAXBContext.newInstance(DeleteTokenReply.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		
		return (DeleteTokenReply) jaxbUnmarshaller.unmarshal(new StringReader(doPost(url)));
	}
}
