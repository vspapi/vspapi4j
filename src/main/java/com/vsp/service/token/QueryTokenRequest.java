package com.vsp.service.token;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.vsp.api.VSCardType;
import com.vsp.api.VSContext;
import com.vsp.service.VSRequest;

@XmlRootElement(name="Request")
public class QueryTokenRequest extends VSRequest {
	
	@XmlElement(name="QueryToken")
	private QueryToken queryToken;

	@XmlTransient
	public String getPath() {
		return "/vsg2/querytoken";
	}
	
	public void validate() {
		//!! Needs to be implemented
	}
	
	public QueryTokenRequest(){
		super();
		this.queryToken = new QueryToken();
	}
	
	public QueryTokenRequest(VSContext ctx) {
		super(ctx);
		this.queryToken = new QueryToken();
	}
	
	@XmlTransient
	public String getCustomId() {
		return queryToken.customId;
	}

	public void setCustomId(String customId) {
		this.queryToken.customId = customId;
	}

	@XmlTransient
	public String getExpirationMonth() {
		return queryToken.expirationMonth;
	}

	public void setExpirationMonth(String expirationMonth) {
		this.queryToken.expirationMonth = expirationMonth;
	}

	@XmlTransient
	public String getExpirationYear() {
		return queryToken.expirationYear;
	}

	public void setExpirationYear(String expirationYear) {
		this.queryToken.expirationYear = expirationYear;
	}

	@XmlTransient
	public String getFirstName() {
		return queryToken.firstName;
	}

	public void setFirstName(String firstName) {
		this.queryToken.firstName = firstName;
	}

	@XmlTransient
	public String getLastName() {
		return queryToken.lastName;
	}

	public void setLastName(String lastName) {
		this.queryToken.lastName = lastName;
	}

	@XmlTransient
	public String getCardType() {
		return queryToken.cardType;
	}

	public void setCardType(VSCardType cardType) {
		this.queryToken.cardType = (cardType == null)? null : cardType.toString();
	}
	
	public QueryTokenReply send(String url) throws Exception {
		JAXBContext jaxbContext = JAXBContext.newInstance(QueryTokenReply.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		
		return (QueryTokenReply) jaxbUnmarshaller.unmarshal(new StringReader(doPost(url)));
	}

}
