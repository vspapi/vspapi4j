package com.vsp.service.token;

import javax.xml.bind.annotation.XmlElement;

class UpdateToken extends CreateToken {

	@XmlElement(name="Token")
	String token;

}
