package com.vsp.service.token;

import javax.xml.bind.annotation.XmlElement;

class Data {
	@XmlElement(name="CustomId")
	String customId;
	
	@XmlElement(name="ExpMonth")
	String expirationMonth;
	
	@XmlElement(name="ExpYear")
	String expirationYear;
	
	@XmlElement(name="FirstName")
	String firstName;
	
	@XmlElement(name="LastName")
	String lastName;
	
	@XmlElement(name="CardType")
	String cardType;
	
	@XmlElement(name="Token")
	String token;
	
}
