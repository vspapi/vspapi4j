package com.vsp.service.token;

import javax.xml.bind.annotation.XmlElement;

class QueryToken {
	
	@XmlElement(name="CustomId")
	String customId;
	
	@XmlElement(name="ExpirationMonth")
	String expirationMonth;
	
	@XmlElement(name="ExpirationYear")
	String expirationYear;
	
	@XmlElement(name="FirstName")
	String firstName;
	
	@XmlElement(name="LastName")
	String lastName;
	
	@XmlElement(name="CardType")
	String cardType;

}
