package com.vsp.service.token;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.vsp.service.VSReply;

@XmlRootElement(name="Reply")
public class QueryTokenReply extends VSReply {
	
	@XmlElement(name="Data")
	private Data data = new Data();
	
	@XmlTransient
	public String getCustomId() {
		return data.customId;
	}

	@XmlTransient
	public String getExpirationMonth() {
		return data.expirationMonth;
	}

	@XmlTransient
	public String getExpirationYear() {
		return data.expirationYear;
	}

	@XmlTransient
	public String getFirstName() {
		return data.firstName;
	}

	@XmlTransient
	public String getLastName() {
		return data.lastName;
	}

	@XmlTransient
	public String getCardType() {
		return data.cardType;
	}

	@XmlTransient
	public String getToken() {
		return data.token;
	}

	public void setCustomId(String customId) {
		this.data.customId = customId;
	}

	public void setExpirationMonth(String expirationMonth) {
		this.data.expirationMonth = expirationMonth;
	}

	public void setExpirationYear(String expirationYear) {
		this.data.expirationYear = expirationYear;
	}

	public void setFirstName(String firstName) {
		this.data.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.data.lastName = lastName;
	}

	public void setCardType(String cardType) {
		this.data.cardType = cardType;
	}

	public void setToken(String token) {
		this.data.token = token;
	}

}
