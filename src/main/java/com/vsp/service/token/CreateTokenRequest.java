package com.vsp.service.token;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.vsp.api.VSContext;
import com.vsp.service.VSRequest;

@XmlRootElement(name="Request")
public class CreateTokenRequest extends VSRequest {

	
	
	@XmlElement(name="CreateToken")
	private CreateToken createToken;
	
	public CreateTokenRequest(){
		super();
		createToken = new CreateToken();
	}
	
	public CreateTokenRequest(VSContext ctx){
		super(ctx);
		createToken = new CreateToken();
	}

	
	@XmlTransient
	public String getAccountNumber() {
		return createToken.accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.createToken.accountNumber = accountNumber;
	}

	@XmlTransient
	public String getApplicationId() {
		return createToken.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.createToken.applicationId = applicationId;
	}

	@XmlTransient
	public String getExpirationMonth() {
		return createToken.expirationMonth;
	}

	public void setExpirationMonth(String expirationMonth) {
		this.createToken.expirationMonth = expirationMonth;
	}

	@XmlTransient
	public String getExpirationYear() {
		return createToken.expirationYear;
	}

	public void setExpirationYear(String expirationYear) {
		this.createToken.expirationYear = expirationYear;
	}

	@XmlTransient
	public String getCardHolderFirstName() {
		return createToken.cardHolderFirstName;
	}

	public void setCardHolderFirstName(String cardHolderFirstName) {
		this.createToken.cardHolderFirstName = cardHolderFirstName;
	}

	@XmlTransient
	public String getCardHolderLastName() {
		return createToken.cardHolderLastName;
	}

	public void setCardHolderLastName(String cardHolderLastName) {
		this.createToken.cardHolderLastName = cardHolderLastName;
	}

	@XmlTransient
	public String getAvsZip() {
		return createToken.avsZip;
	}

	public void setAvsZip(String avsZip) {
		this.createToken.avsZip = avsZip;
	}

	@XmlTransient
	public String getAvsStreet() {
		return createToken.avsStreet;
	}

	public void setAvsStreet(String avsStreet) {
		this.createToken.avsStreet = avsStreet;
	}

	@XmlTransient
	public String getCustomId() {
		return createToken.customId;
	}

	public void setCustomId(String customId) {
		this.createToken.customId = customId;
	}

	@Override
	@XmlTransient
	public String getPath() {
		return "/vsg2/createtoken";
	}
	
	public void validate() {
		//!! Needs to be implemented
	}
	
	public CreateTokenReply send(String url) throws Exception {
		JAXBContext jaxbContext = JAXBContext.newInstance(CreateTokenReply.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		
		String resp = doPost(url);
		
		return (CreateTokenReply) jaxbUnmarshaller.unmarshal(new StringReader(resp));
	}
}
