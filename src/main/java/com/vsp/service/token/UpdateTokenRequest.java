package com.vsp.service.token;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.vsp.api.VSContext;
import com.vsp.service.VSRequest;

/**
 * The Update Expiration Request message updates the card data expiration data 
 * associated with the given token, based on the parameters supplied.
 * 
 * @author Sierra Skyport
 */
@XmlRootElement(name="Request")
public class UpdateTokenRequest extends VSRequest {
	
	@XmlElement(name="CreateToken")
	UpdateToken updateToken;

	public UpdateTokenRequest() {
		super();
		updateToken = new UpdateToken();
	}

	public UpdateTokenRequest(VSContext ctx) {
		super(ctx);
		updateToken = new UpdateToken();
	}

	@XmlTransient
	@Override
	public String getPath() {
		return "/vsg2/updatetoken";
	}
	
	public void validate() {
		//!! Needs to be implemented
	}
	
	@XmlTransient
	public String getToken() {
		return updateToken.applicationId;
	}

	public void setToken(String token) {
		this.updateToken.token = token;
	}
	

	@XmlTransient
	public String getApplicationId() {
		return updateToken.applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.updateToken.applicationId = applicationId;
	}

	@XmlTransient
	public String getExpirationMonth() {
		return updateToken.expirationMonth;
	}

	public void setExpirationMonth(String expirationMonth) {
		this.updateToken.expirationMonth = expirationMonth;
	}

	@XmlTransient
	public String getExpirationYear() {
		return updateToken.expirationYear;
	}

	public void setExpirationYear(String expirationYear) {
		this.updateToken.expirationYear = expirationYear;
	}

	@XmlTransient
	public String getAvsZip() {
		return updateToken.avsZip;
	}

	public void setAvsZip(String avsZip) {
		this.updateToken.avsZip = avsZip;
	}

	@XmlTransient
	public String getAvsStreet() {
		return updateToken.avsStreet;
	}

	public void setAvsStreet(String avsStreet) {
		this.updateToken.avsStreet = avsStreet;
	}
	
	public CreateTokenReply send(String url) throws Exception {
		JAXBContext jaxbContext = JAXBContext.newInstance(CreateTokenReply.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		
		String resp = doPost(url);
		
		return (CreateTokenReply) jaxbUnmarshaller.unmarshal(new StringReader(resp));
	}
	
	

}
