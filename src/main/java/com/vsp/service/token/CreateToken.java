package com.vsp.service.token;

import javax.xml.bind.annotation.XmlElement;

/**
 * Create Token
 * 
 * The Create Token message accepts and stores encrypted credit card account information. A successful 
 * message returns a token that can be used to process future transactions.
 * 
 * The token generated is a 16 digit number, uniquely identifying the card information within the 
 * vSecureProcessing system. The last 4 digits of the token include the last 4 digits of the account number. 
 * 
 * The token is tied to the account that created the token. The token can only be used to process a 
 * transaction with the account that the token was created for.
 * 
 * A token will be store on the vSecureProcessing system for up to 1 year from the last time the token was 
 * accessed or until the expiration date of the account number is reached. 
 * 
 * For example, each time a token is accessed (either to process a transaction or with an update command), 
 * the token expiration date is extended 1 year from that moment.
 * 
 * Unless stated otherwise, all fields are required.
 * @author SierraSkyport 
 *
 */

class CreateToken {
	
	/**
	 * Credit card number. Only numeric characters are allowed; spaces and hyphens are not allowed; 
	 * for example; 41111111111111111 is allowed, 4111-1111-1111-1111 is not allowed.
	 * 
	 * Max Size: 19
	 */
	@XmlElement(name="AccountNumber")
	String accountNumber;
	
	/**
	 * Application identifier for the application used in sending/receiving transaction request. 
	 * The value of this field is assigned/authorized by the gateway and must be used in all 
	 * transactions used by the certified application.
	 * 
	 * Max Size: 20
	 */
	@XmlElement(name="ApplicationId")
	String applicationId;
	
	/**
	 * Credit card expiration month in MM format. Exactly two characters required; for example, 02
	 * 
	 * Max Size: 2
	 */
	@XmlElement(name="ExpirationMonth")
	String expirationMonth;
	
	/**
	 * Credit card expiration year in YY format. Exactly two characters required; for example, 14.
	 * 
	 * Max Size: 2
	 */
	@XmlElement(name="ExpirationYear")
	String expirationYear;
	
	/**
	 * Optional. Card holder first name.
	 * 
	 * Max Size: 40
	 */
	@XmlElement(name="CardHolderFirstName")
	String cardHolderFirstName;
	
	/**
	 * Optional. Card holder last name.
	 * 
	 * Max Size: 40
	 */
	@XmlElement(name="CardHolderLastName")
	String cardHolderLastName;
	
	
	/**
	 * Optional. Billing ZIP or postal code used for AVS.
	 * 
	 * Max Size: 10
	 */
	@XmlElement(name="AvsZip")
	String avsZip;
	
	/**
	 * Optional. Billing street address used for AVS.
	 * 
	 * Max Size: 80
	 */	
	@XmlElement(name="AvsStreet")
	String avsStreet;
	
	/**
	 * An ID that uniquely identifies the token.
	 * 
	 * Max Size: 40
	 */
	@XmlElement(name="CustomId")
	String customId;
	
}
