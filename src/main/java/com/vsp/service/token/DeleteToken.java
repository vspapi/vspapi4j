package com.vsp.service.token;

import javax.xml.bind.annotation.XmlElement;

class DeleteToken {

	@XmlElement(name="Token")
	String token;
	
	public DeleteToken() {
		super();
	}
	
	public DeleteToken(String token) {
		super();
		this.token = token;
	}
	
}
